#pragma once

//////////////////////////////////////////////////////////////////////////
// ### Default Ymir Macros ###
#define LOCALE_SERVICE_EUROPE
#define ENABLE_COSTUME_SYSTEM
#define ENABLE_ENERGY_SYSTEM
#define ENABLE_DRAGON_SOUL_SYSTEM
#define ENABLE_NEW_EQUIPMENT_SYSTEM
// ### Default Ymir Macros ###
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// ### New From LocaleInc ###
#define ENABLE_PACK_GET_CHECK
#define ENABLE_CANSEEHIDDENTHING_FOR_GM
#define ENABLE_PROTOSTRUCT_AUTODETECT

#define ENABLE_PLAYER_PER_ACCOUNT5
#define ENABLE_LEVEL_IN_TRADE
#define ENABLE_DICE_SYSTEM
#define ENABLE_EXTEND_INVEN_SYSTEM
#define ENABLE_LVL115_ARMOR_EFFECT

#define WJ_SHOW_MOB_INFO
#ifdef WJ_SHOW_MOB_INFO
#define ENABLE_SHOW_MOBAIFLAG
#define ENABLE_SHOW_MOBLEVEL
#endif
// ### New From LocaleInc ###
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// ### From GameLib ###
#define ENABLE_WOLFMAN_CHARACTER

// #define ENABLE_MAGIC_REDUCTION_SYSTEM
#define ENABLE_MOUNT_COSTUME_SYSTEM
#define ENABLE_WEAPON_COSTUME_SYSTEM
// ### From GameLib ###
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// ### New Ymir Macros ###
#define NEW_SELECT_CHARACTER
#define WJ_ENABLE_PICKUP_ITEM_EFFECT
#define ENABLE_ACCE_COSTUME_SYSTEM
#define ENABLE_ACCE_SECOND_COSTUME_SYSTEM
#define ENABLE_OFFLINE_SHOP_SYSTEM
#define ENABLE_QUIVER_SYSTEM
#define ENABLE_MULTI_LANGUAGE_SYSTEM
#define ENABLE_WHISPER_ADMIN_SYSTEM
#define ENABLE_MOVE_CHANNEL
#define ENABLE_KEYCHANGE_SYSTEM
#define ENABLE_ENVIRONMENT_EFFECT_OPTION
#define ENABLE_FOG_FIX
#define ENABLE_PORTABLE_BUFF_SYSTEM
#define WJ_ENABLE_TRADABLE_ICON
#define ENABLE_TAB_NEXT_TARGET
#define ENABLE_QUEST_CATEGORY
// ### New Ymir Macros ###
//////////////////////////////////////////////////////////////////////////
