#include "StdAfx.h"
#include "PythonNetworkStream.h"

void CPythonNetworkStream::OnRemoteDisconnect()
{
	PyCallClassMemberFunc(m_poHandler, "SetLoginPhase", Py_BuildValue("()"));
}

void CPythonNetworkStream::OnDisconnect()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Game
void CPythonNetworkStream::OnScriptEventStart(int iSkin, int iIndex)
{
	PyCallClassMemberFunc(m_apoPhaseWnd[PHASE_WINDOW_GAME], "OpenQuestWindow", Py_BuildValue("(ii)", iSkin, iIndex));
}

#ifdef ENABLE_KEYCHANGE_SYSTEM
void CPythonNetworkStream::OpenKeyChangeWindow()
{
	PyCallClassMemberFunc(m_apoPhaseWnd[PHASE_WINDOW_GAME], "OpenKeyChangeWindow", Py_BuildValue("()"));
}
#endif

void CPythonNetworkStream::OnQuestScriptEventStart(int iSkin, int iIndex)
{
#ifndef USE_SLIDE_QUEST_WINDOW
	OnScriptEventStart(iSkin, iIndex);
#else
	PyCallClassMemberFunc(m_apoPhaseWnd[PHASE_WINDOW_GAME], "OpenQuestSlideWindow", Py_BuildValue("(ii)", iSkin, iIndex));
#endif
}
