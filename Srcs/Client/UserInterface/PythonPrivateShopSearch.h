#pragma once

class CPythonPrivateShopSearch : public CSingleton<CPythonPrivateShopSearch>
{
	public:
		struct TSearchItemData : TItemData
		{
			DWORD vid;
			int price;
			TItemPos Cell;
		};

		typedef std::vector<TSearchItemData> TItemInstanceVector;

	public:
		CPythonPrivateShopSearch();
		virtual ~CPythonPrivateShopSearch();

		void AddItemData(const TSearchItemData & rItemData);
		void ClearItemData();

		DWORD GetItemDataCount() { return m_ItemInstanceVector.size(); }
		DWORD GetItemDataPtr(DWORD index, TSearchItemData ** ppInstance);

	protected:
		TItemInstanceVector m_ItemInstanceVector;
};
