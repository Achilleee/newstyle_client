/Srcs/Tools/Metin2MSAMaker is currently using the compiled libraries
system and filesystem from the boost framework.

This could be boring for someone, so I decided to not include such libraries
inside Extern.rar because they also are based on the compiler version.

If you want to use them, you should download the relative files from:
http://sourceforge.net/projects/boost/files/boost-binaries/1.43.0/
*libboost_filesystem-vc100-mt-s-1_43.zip
*libboost_system-vc100-mt-s-1_43.zip

If you're using a version higher than vs2010 as compiler, you should either
manually compile the boost libraries or upgrade the boost library folder and take
the newer ones from your updated boost version:
http://sourceforge.net/projects/boost/files/boost-binaries/XXX/
